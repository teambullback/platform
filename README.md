server
======

server for webbles

venv를 이용한 개발환경 이식
-------

```sh
virtualenv --distribute env
source env/bin/activate
pip install -r requirements.txt
python manage.py syncdb
python manage.py runserver
```

venv를 이용한 개발환경 구축
-------

virtualenv 안에 설치된 패키지 목록을 requirements.txt 로 export 하기

```sh
pip freeze > requirements.txt
```