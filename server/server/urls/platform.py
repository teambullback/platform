# -*- coding:utf-8 -*-

from django.conf.urls import patterns, url
from ..views.platform import *

urls_platform = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^intro/$', IntroView.as_view(), name='intro'),
    url(r'^categories/$', CategoryListView.as_view(), name='category-list'),
    url(r'^categories/(?P<pk>\d+)/$', CategoryDetailView.as_view(), name='category-detail'),
    url(r'^tutorials/$', TutorialListView.as_view(), name='tutorial-list'),
    url(r'^tutorials/(?P<pk>\d+)/$', TutorialDetailView.as_view(), name='tutorial-detail'),
    url(r'^tutorials/(?P<pk>\d+)/init/$', TutorialInitializeView.as_view(), name='tutorial-initialize'),
)

