# -*- coding:utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from ..urls.platform import urls_platform
from ..views import *

admin.autodiscover()

router = DefaultRouter()
router.register(r'sites', SiteViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'tutorials', TutorialViewSet)
router.register(r'feedbacks', FeedbackViewSet)
router.register(r'records', RecordViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'likes', LikeViewSet)

urlpatterns = patterns('',
    url(r'^', include(urls_platform)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-list/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^api-token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),
    url(r'^docs/', include('rest_framework_swagger.urls')),
)