# -*- coding:utf-8 -*-
from ..models import *


def retrieve_or_create_member(request):
    ip_address = request.META['REMOTE_ADDR']

    try:
        member = AnonymousMember.objects.get(ip_address=ip_address)

    except AnonymousMember.DoesNotExist:
        member = AnonymousMember(ip_address=ip_address)
        member.save()

    return member


def record_member_access(request, **kwargs):
    try:
        member = retrieve_or_create_member(request)

        # access_at, 'platform', 'extension'
        try:
            access_at = kwargs['access_at']

        except Exception, e:
            print e
            access_at = 'extension'

        # access_on, 'get', 'put', 'post', 'patch', 'delete'
        try:
            access_on = kwargs['access_on']

        except Exception, e:
            print e
            access_on = 'get'

        # target_model, if there are no model access then save as ''
        try:
            target_model = kwargs['target_model']

        except Exception, e:
            print e
            target_model = ''

        # target_model, if there are no model access then save as ''
        try:
            target_index = kwargs['target_index']

        except Exception, e:
            print e
            target_index = ''

        try:
            action = kwargs['action']

        except Exception, e:
            print e
            action = ''

        record = Record(member=member, access_at=access_at, access_on=access_on, target_model=target_model,
                        target_index=target_index, action=action)
        record.save()
        return True

    except Exception, e:
        print e
        return False