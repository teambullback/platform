# -*- coding:utf-8 -*-

from os import environ


def common_context_values(context):
    if environ['DJANGO_SETTINGS_MODULE'] == 'server.settings.deploy':
        context['is_server'] = True

    return context
