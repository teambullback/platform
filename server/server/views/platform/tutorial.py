# -*- coding:utf-8 -*-

from hashlib import md5
from random import sample
from django.views.generic import DetailView, ListView, TemplateView
from django.db.models import Q
from ...controllers import *
from ...constants import hexa_icon, hexa_word


class IndexView(TemplateView):
    template_name = 'server/index.html'

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view index page')

        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        categories = Category.objects.all()
        random_int = sample(xrange(len(categories)), 3)

        targets = []
        for i in range(3):
            targets.append(categories[random_int[i]])

        context['categories'] = targets

        n_member = len(AnonymousMember.objects.all())
        n_tutorial = len(Tutorial.objects.all())
        n_initialize = len(Record.objects.filter(action='init'))

        context['n_member'] = n_member
        context['n_tutorial'] = n_tutorial
        context['n_initialize'] = n_initialize

        context = common_context_values(context)

        return context


class IntroView(TemplateView):
    template_name = 'server/intro.html'

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view intro page')

        return super(IntroView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IntroView, self).get_context_data(**kwargs)

        context = common_context_values(context)

        return context


class CategoryListView(ListView):
    model = Category

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view category-list page')

        return super(CategoryListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['categories'] = self.get_queryset()

        context = common_context_values(context)

        return context


class CategoryDetailView(DetailView):
    model = Category

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view category-detail page')

        return super(CategoryDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context['category'] = self.get_object()

        context = common_context_values(context)

        return context


class TutorialListView(ListView):
    model = Tutorial
    template_name = 'server/tutorial_list.html'

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view tutorial-list page')

        return super(TutorialListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        q = self.request.GET.get('q', False)
        a = self.request.GET.get('a', False)

        if q:
            queryset = self.model.objects.filter(Q(is_finish=True) & (Q(title__contains=q) | Q(intro__contains=q) |
                                                                      Q(description__contains=q)))
        else:
            queryset = self.model.objects.filter(is_finish=True)

        if a == 'v':
            return sorted(queryset, reverse=True, key=lambda t: t.amount_views)

        elif a == 'r':
            return sorted(queryset, reverse=True, key=lambda t: t.amount_reviews)

        else:
            return sorted(queryset, reverse=True, key=lambda t: t.amount_likes)

    def get_context_data(self, **kwargs):
        context = super(TutorialListView, self).get_context_data(**kwargs)
        context['tutorials'] = self.get_queryset()
        context['q'] = self.request.GET.get('q', False)
        context['a'] = self.request.GET.get('a', False)

        context = common_context_values(context)

        return context


class TutorialDetailView(DetailView):
    model = Tutorial

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view tutorial-detail page')

        return super(TutorialDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        def give_anonymous_nick(reviews):
            users = []
            faicons = []
            verbals = []

            for i, review in enumerate(reviews):
                if review.created_by in users:
                    review.faicon = faicons[users.index(review.created_by)]
                    review.verbal = verbals[users.index(review.created_by)]

                else:
                    hashed = md5(str(i) + str(tutorial.pk)).hexdigest()[-2:]
                    review.faicon = hexa_icon[hashed[0]]['icon']
                    review.verbal = hexa_word[hashed[1]]['verbal'] + " " + hexa_icon[hashed[0]]['verbal']
                    users.append(review.created_by)
                    faicons.append(review.faicon)
                    verbals.append(review.verbal)

            return reviews

        tutorial = self.get_object()
        context = super(TutorialDetailView, self).get_context_data(**kwargs)
        context['tutorial'] = self.get_object()
        print tutorial.reviews.all()
        context['reviews'] = give_anonymous_nick(tutorial.reviews.all())

        context = common_context_values(context)

        return context


class TutorialInitializeView(DetailView):
    model = Tutorial
    template_name = 'server/tutorial_initialize.html'

    def dispatch(self, request, *args, **kwargs):
        record_member_access(request, access_at='platform', action='view tutorial-initialize page')

        return super(TutorialInitializeView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TutorialInitializeView, self).get_context_data(**kwargs)
        context['tutorial'] = self.get_object()

        context = common_context_values(context)

        return context
