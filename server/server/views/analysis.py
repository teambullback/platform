# -*- coding:utf-8 -*-

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework.permissions import AllowAny
from ..controllers import *
from ..serializers import *
from ..permissions import *



class FeedbackViewSet(viewsets.ModelViewSet):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = (IsSafeOrPost,)

    def pre_save(self, obj):
        obj.created_by = retrieve_or_create_member(self.request)
        obj.updated_by = retrieve_or_create_member(self.request)

    def create(self, request, *args, **kwargs):
        record_member_access(request, access_on='post', target_model='Feedback', action='create feedback')

        return super(FeedbackViewSet, self).create(request, *args, **kwargs)


class RecordViewSet(viewsets.ModelViewSet):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
    permission_classes = (IsSafeOrPost,)

    def pre_save(self, obj):
        obj.created_by = retrieve_or_create_member(self.request)
        obj.updated_by = retrieve_or_create_member(self.request)


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (IsSafeOrPost,)

    def pre_save(self, obj):
        obj.created_by = retrieve_or_create_member(self.request)
        obj.updated_by = retrieve_or_create_member(self.request)

    def create(self, request, *args, **kwargs):
        record_member_access(request, access_on='post', target_model='Review', action='create review')

        return super(ReviewViewSet, self).create(request, *args, **kwargs)


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = (IsSafeOrPost,)

    def pre_save(self, obj):
        obj.created_by = retrieve_or_create_member(self.request)
        obj.updated_by = retrieve_or_create_member(self.request)

    def create(self, request, *args, **kwargs):
        record_member_access(request, access_on='post', target_model='Like', action='create like')

        user = retrieve_or_create_member(request)
        tutorial = Tutorial.objects.get(pk=request.POST.get('tutorial', False))

        try:
            like = Like.objects.get(tutorial=tutorial, created_by=user)

        except Like.DoesNotExist:
            return super(LikeViewSet, self).create(request, *args, **kwargs)

        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @list_route(methods=['post'], permission_classes=[IsOwnerOrReadOnly])
    def cancel(self, request,  *args, **kwargs):
        record_member_access(request, access_on='delete', target_model='Like', action='delete like')

        user = retrieve_or_create_member(request)
        tutorial = Tutorial.objects.get(pk=request.POST.get('tutorial', False))

        try:
            like = Like.objects.get(tutorial=tutorial, created_by=user)

        except Like.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        else:
            like.delete()
            return Response(status=status.HTTP_200_OK)