# -*- coding:utf-8 -*-

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework.permissions import *
from ..controllers import *
from ..serializers import *
from ..permissions import *


class SiteViewSet(viewsets.ModelViewSet):
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    permission_classes = (IsAdminOrReadOnly,)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.filter()
    serializer_class = CategorySerializer
    permission_classes = (IsAdminOrReadOnly,)

    @detail_route(methods=['get'])
    def list_tutorial(self, request, pk):
        category = Category.objects.get(pk=pk)
        tutorials = Tutorial.objects.filter(category=category)

        return Response(TutorialSerializer(tutorials, many=True).data)


class TutorialViewSet(viewsets.ModelViewSet):
    queryset = Tutorial.objects.filter(is_finish=True)
    serializer_class = TutorialSerializer
    permission_classes = (IsAdminOrReadOnly,)

    def pre_save(self, obj):
        obj.created_by = self.request.user
        obj.updated_by = self.request.user

    def retrieve(self, request, *args, **kwargs):
        self.queryset = Tutorial.objects.filter()

        return super(TutorialViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        self.queryset = Tutorial.objects.filter()

        return super(TutorialViewSet, self).partial_update(request, *args, **kwargs)

    @list_route(methods=['get'])
    def unfinished(self, request):
        tutorials = Tutorial.objects.all()

        return Response(TutorialSerializer(tutorials, many=True).data)

    @detail_route(methods=['get'])
    def list_review(self, request, pk):
        tutorial = Tutorial.objects.get(pk=pk)
        reviews = Review.objects.filter(tutorial=tutorial)

        return Response(ReviewSerializer(reviews, many=True).data)

    @detail_route(methods=['get'])
    def is_user_like_it(self, request, pk, *args, **kwargs):

        user = retrieve_or_create_member(request)
        tutorial = Tutorial.objects.get(pk=pk)

        try:
            like = Like.objects.get(tutorial=tutorial, created_by=user)

        except Like.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['get'])
    def init(self, request, pk):
        record_member_access(request, access_at='extension', access_on='get', target_model='tutorial', target_index=pk,
                             action='init')

        return Response(status=status.HTTP_200_OK)

    @detail_route(methods=['get'])
    def term(self, request, pk):

        record_member_access(request, access_at='extension', access_on='get', target_model='tutorial', target_index=pk,
                             action='term')

        return Response(status=status.HTTP_200_OK)