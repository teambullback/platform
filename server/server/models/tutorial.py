# -*- coding:utf-8 -*-

from django.db import models


class Site(models.Model):
    class Meta:
        app_label = 'server'

    title = models.CharField(max_length=32)
    description = models.TextField()


class Category(models.Model):
    class Meta:
        app_label = 'server'

    title = models.CharField(max_length=32, blank=True)
    description = models.TextField(blank=True)

    @property
    def initial_tutorial(self):
        from .tutorial import Tutorial
        try:
            return Tutorial.objects.get(category=self, index=1).id
        except Exception, e:
            print e
            return None

    @property
    def total_amount_views(self):
        from .analysis import Record
        from .tutorial import Tutorial

        count = 0

        try:
            tutorials = Tutorial.objects.get(category=self)
            for tutorial in tutorials:
                count += len(Record.object.filter(access_at='extension', access_on='get', target_model='tutorial',
                                                  target_index=tutorial.pk))

        except Tutorial.DoesNotExist:
            pass

        except Record.DoesNotExist:
            pass

        return count


class Tutorial(models.Model):
    class Meta:
        app_label = 'server'

    index = models.IntegerField(default=0)
    title = models.CharField(max_length=32, blank=True)
    intro = models.TextField(blank=True)
    contents = models.TextField(blank=True)
    description = models.TextField(blank=True)

    is_finish = models.BooleanField(default=False)
    is_public = models.BooleanField(default=False)
    is_hidden = models.BooleanField(default=False)

    req_login = models.BooleanField(default=False)
    url_login = models.TextField(blank=True)

    created_by = models.ForeignKey('auth.User', null=True, blank=True, related_name='create_tutorials')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey('auth.User', null=True, blank=True, related_name='update_tutorials')
    updated_on = models.DateTimeField(auto_now=True)

    site = models.ForeignKey(Site, related_name='tutorials')
    category = models.ForeignKey(Category, null=True, blank=True, related_name='tutorials')

    @property
    def amount_views(self):
        from .analysis import Record
        return Record.objects.filter(target_model='tutorial', target_index=str(self.pk), action='init').count()

    @property
    def amount_likes(self):
        from .analysis import Like
        return Like.objects.filter(tutorial=self).count()

    @property
    def amount_reviews(self):
        from .analysis import Review
        return Review.objects.filter(tutorial=self).count()

    @property
    def next_tutorial_at_category(self):
        if self.index:
            try:
                return Tutorial.objects.get(category=self.category, index=(self.index + 1)).id
            except Exception, e:
                print e
                return None

        else:
            return None

    @property
    def prev_tutorial_at_category(self):
        if self.index:
            try:
                return Tutorial.objects.get(category=self.category, index=(self.index - 1)).id
            except Exception, e:
                print e
                return None

        else:
            return None

    @property
    def initial_url(self):
        tmp = self.contents.index("http")

        url = self.contents[tmp:]
        url = url[:url.index("\\")]

        return url