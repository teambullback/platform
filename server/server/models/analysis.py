# -*- coding:utf-8 -*-

from django.db import models


class Feedback(models.Model):
    class Meta:
        app_label = 'server'

    mail = models.TextField()
    contents = models.TextField()

    created_by = models.ForeignKey('server.AnonymousMember', null=True, blank=True, related_name='create_feedbacks')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey('server.AnonymousMember', null=True, blank=True, related_name='update_feedbacks')
    updated_on = models.DateTimeField(auto_now=True)


class Record(models.Model):
    class Meta:
        app_label = 'server'

    time = models.DateTimeField(auto_now_add=True)
    member = models.ForeignKey('server.AnonymousMember', related_name='records')
    access_at = models.CharField(max_length=32)
    access_on = models.CharField(max_length=32)
    target_model = models.CharField(max_length=32, blank=True)
    target_index = models.CharField(max_length=32, blank=True)
    action = models.CharField(max_length=32)

    @classmethod
    def create(cls, member, access_at, access_on, target_model, target_index, action):
        return cls(member=member, access_at=access_at, access_on=access_on, target_model=target_model,
                   target_index=target_index, action=action)


class Review(models.Model):
    class Meta:
        app_label = 'server'

    contents = models.TextField()

    tutorial = models.ForeignKey('server.Tutorial', related_name='reviews')

    created_by = models.ForeignKey('server.AnonymousMember', null=True, blank=True, related_name='create_reviews')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey('server.AnonymousMember', null=True, blank=True, related_name='update_reviews')
    updated_on = models.DateTimeField(auto_now=True)


class Like(models.Model):
    class Meta:
        app_label = 'server'

    tutorial = models.ForeignKey('server.Tutorial', related_name='likes')
    created_by = models.ForeignKey('server.AnonymousMember', null=True, blank=True, related_name='create_likes')
    created_on = models.DateTimeField(auto_now_add=True)