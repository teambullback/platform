# -*- coding:utf-8 -*-

from django.db import models


class AnonymousMember(models.Model):
    class Meta:
        app_label = 'server'

    ip_address = models.CharField(max_length=15)

    @classmethod
    def create(cls, ip_address):
        return cls(ip_address=ip_address)