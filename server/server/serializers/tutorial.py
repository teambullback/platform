# -*- coding:utf-8 -*-

from rest_framework import serializers
from ..models import *


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = ('id', 'title', 'description', 'tutorials')


class CategorySerializer(serializers.ModelSerializer):
    initial_tutorial = serializers.Field()

    class Meta:
        model = Category
        fields = ('id', 'title', 'description', 'initial_tutorial', 'tutorials')


class TutorialSerializer(serializers.ModelSerializer):
    amount_views = serializers.Field()
    amount_likes = serializers.Field()
    amount_reviews = serializers.Field()
    prev_tutorial_at_category = serializers.Field()
    next_tutorial_at_category = serializers.Field()

    class Meta:
        model = Tutorial
        fields = ('id', 'index', 'title', 'intro', 'contents', 'description', 'amount_views', 'amount_likes',
                  'amount_reviews', 'is_finish', 'is_public', 'is_hidden', 'req_login', 'url_login', 'created_by',
                  'created_on', 'updated_by', 'updated_on', 'site', 'category', 'prev_tutorial_at_category',
                  'next_tutorial_at_category')