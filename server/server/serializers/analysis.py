# -*- coding:utf-8 -*-

from rest_framework import serializers
from ..models import *


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('id', 'mail', 'contents', 'created_by', 'created_on', 'updated_by', 'updated_on')


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('id', 'time', 'member', 'access_at', 'access_on', 'target_model', 'target_index', 'action')


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'contents', 'tutorial', 'created_by', 'created_on', 'updated_by',
                  'updated_on')


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('id', 'tutorial', 'created_by', 'created_on')