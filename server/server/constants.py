# -*- coding:utf-8 -*-

hexa_icon = {
    '0': {
        'icon': 'fa-car',
        'verbal': '자동차',
    },
    '1': {
        'icon': 'fa-bicycle',
        'verbal': '자전거',
    },
    '2': {
        'icon': 'fa-binoculars',
        'verbal': '쌍안경',
    },
    '3': {
        'icon': 'fa-child',
        'verbal': '꼬꼬마',
    },
    '4': {
        'icon': 'fa-trophy',
        'verbal': '트로피',
    },
    '5': {
        'icon': 'fa-trash',
        'verbal': '휴지통',
    },
    '6': {
        'icon': 'fa-fighter-jet',
        'verbal': '제트기',
    },
    '7': {
        'icon': 'fa-frown-o',
        'verbal': '시무룩',
    },
    '8': {
        'icon': 'fa-calculator',
        'verbal': '계산기',
    },
    '9': {
        'icon': 'fa-camera-retro',
        'verbal': '카메라',
    },
    'a': {
        'icon': 'fa-beer',
        'verbal': '맥주잔',
    },
    'b': {
        'icon': 'fa-cloud',
        'verbal': '비구름',
    },
    'c': {
        'icon': 'fa-birthday-cake',
        'verbal': '케이크',
    },
    'd': {
        'icon': 'fa-eraser',
        'verbal': '지우개',
    },
    'e': {
        'icon': 'fa-leaf',
        'verbal': '나뭇잎',
    },
    'f': {
        'icon': 'fa-bomb',
        'verbal': '폭탄맨',
    },
}

hexa_word = {
    '0': {
        'verbal': '멍청한',
    },
    '1': {
        'verbal': '똑똑한',
    },
    '2': {
        'verbal': '귀여운',
    },
    '3': {
        'verbal': '상큼한',
    },
    '4': {
        'verbal': '잉여한',
    },
    '5': {
        'verbal': '얍삽한',
    },
    '6': {
        'verbal': '무서운',
    },
    '7': {
        'verbal': '더러운',
    },
    '8': {
        'verbal': '거대한',
    },
    '9': {
        'verbal': '깨끗한',
    },
    'a': {
        'verbal': '부유한',
    },
    'b': {
        'verbal': '가난한',
    },
    'c': {
        'verbal': '온전한',
    },
    'd': {
        'verbal': '망가진',
    },
    'e': {
        'verbal': '비참한',
    },
    'f': {
        'verbal': '맹렬한',
    },
}