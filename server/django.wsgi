import os
import sys
sys.path.append('/var/www/server/server')
os.environ['DJANGO_SETTINGS_MODULE'] = 'server.settings.deploy'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
